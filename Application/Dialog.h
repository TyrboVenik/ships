#pragma once
#include "..\Konvoi\Konvoi.h"
#include <iostream>


class Dialog 
{
private:
	Konvoi k;
	string Dialog_Str [10];
	//void(Dialog::*mas[10])()= { NULL,D_Exit };
	
	//void(Dialog::*Dialog_Func)() = &Dialog::Exit;
	//void(*Dialog::Dialog_Func[10])();
public:
	Dialog(Konvoi&);

	int Dialog_Num(int);

	void Dialog_Start();
	
	void D_Load();
	void D_AddShip();
	void D_DeleteShip();
	void D_Show_All();
	void D_Konvoi();
	void D_Exit();
	
};

