#include "stdafx.h"
#include "Dialog.h"
#include <vector>
#include<iostream>
using namespace std;

int getNum(int &a)
{
	cin >> a;
	if (!cin.good())
	{
		cin.clear();
		return 0;
	}
	return 1;
}

Dialog::Dialog(Konvoi&K)
{
	k = K;
	Dialog_Str[0] = "0. Exit";
	Dialog_Str[1] = "1. Add Ship";
	Dialog_Str[2] = "2. Delete Ship";
	//Dialog_Str[3] = "3. Load";
	Dialog_Str[3] = "3. Show information about ships";
	Dialog_Str[4] = "5. Show information abot convoy";

}

int Dialog::Dialog_Num(int i)
{
	cout << "Switch:" << endl;
	for (int j = 0; j < 5; ++j)
		cout << Dialog_Str[j] << endl;

	cin >> i;

	switch (i)
	{
	case 0:
		D_Exit();
		break;
	case 1:
		D_AddShip();
		break;
	case 2:
		D_DeleteShip();
		break;
	//case 3:
		//D_Load();
		//break;
	case 3:
		D_Show_All();
		break;
	case 4:
		D_Konvoi();
		break;
	default:
		break;
	}

	return i;
}

void Dialog::Dialog_Start()
{
	int i = 0,a;
	string str;
	cout << "Start..." << endl;
	cout << "Enter information about konvoi captain: " << endl;

	cout << "enter Rang:";

	cin >> str;
	

	cout << endl << "enter Captain name:";
	cin >> str;
	k.getAdmiral().setCaptain_Name(str);

	cout << endl << "enter Captain experince:";
	cin >> a;
	k.getAdmiral().setExp(a);

	cout << endl << "Enter name of start point:";
	cin >> str;
	k.setFrom(str);

	cout << endl << "Enter name of finish point:";
	cin >> str;
	k.setTo(str);

	cout << endl << "Enter Distation to finish";
	cin >> a;
	cout << endl;
	
	k.setDistation(a);

	while (Dialog_Num(i));
}

void Dialog::D_Load()
{
	float z = 5;
	k.Load(z);
}

void Dialog::D_AddShip()
{
	int i;
	
	cout << "Press 1-to Transport, 2-to Protection,3-to Military transport,0-to cancel" << endl;
	cin >> i;

	Ship* S=nullptr;
	string str,cs;
	int a;
	float fl;

	switch (i)
	{
	case 1:
		S = new Transport;
		cout << "T";
		break;
	case 2:
		S = new Protection;
		cout << "T";
		break;
	case 3:
		S = new Military_Transport;
		cout << "T";
		break;
	default:
		break;
	}
	
	cout << "enter Captain:" << endl;
	cout << "enter Rang:";
	
	cin >> str;
	S->getCaptain().setRang(str);
	
	cout <<endl<< "enter Captain name:";
	cin >> str;
	S->getCaptain().setCaptain_Name(str);
	
	cout << endl << "enter Captain experince:";
	
	int rc=1;
	while (rc)
	{
		rc = 0;
		try {
			cin >> a;
			S->getCaptain().setExp(a);
		}
		catch (const char* msg)
		{

			cout << "enter again" << endl;
			rc = 1;
		}
	}
	cout << "enter Ship information:" << endl;
	cout << "Enter ship name:";
	cin >> str;
	S->setShip_Name(str);
	
	rc = 1;
	while (rc)
	{
		rc = 0;
		cout << endl << "Enter callsing:";
		cin >> cs;
		Table T = k.getTab();
		map <string, Descriptor> Map = T.getMap();
		if (Map.count(cs))
		{
			rc = 1;
			cout << "repeat enter" << endl;
		}
	}

	cout <<endl<< "Enter Displacement";
	cin >> fl;
	S->setDis(fl);

	cout << endl << "Enter Max velocity:";
	cin >> fl;
	S->setVel(fl);

	cout << endl << "Enter size of team:" ;
	cin >> a;
	S->setTeam(a);
	cout << endl;
	Transport *T=nullptr;
	Protection *P=nullptr;
	Military_Transport *M_T=nullptr;

	if (i == 2 || i==3)
	{
		Weapon W;
		vector<Weapon> vecW = {W,W,W,W};
		P = dynamic_cast<Protection*>(S);
		cout << endl << "Enter weapon information:" << endl;
		for (int i = 0; i < 4; ++i)
		{
			cout << "Information about weapon(0- no weapon):" << endl;
			cout << "Caliber:";
			cin >> a;
			if (!a)
				continue;
			W.setCaliber(a);

			cout << "Fire range:";
			cin >> a;
			W.setFiring_Range(a);

			cout << "Ammunition:";
			cin >> a;
			W.setAmmunition(a);

			W.setLocation(i);
			vecW[i] = W;
		}
		P->setArms(vecW);
	}
	if (i == 1)
	{
		T = dynamic_cast<Transport*>(S);
		cout << endl << "Enter Load Weight:";
		cin >> fl;
		T->setWeight(fl);
	}
	if (i == 3)
	{
		M_T = dynamic_cast<Military_Transport*>(P);
		cout << endl << "Enter Load Weight:";
		cin >> fl;
		M_T->setWeight(fl);
	}
		
	switch (i)
	{
	case 1:
		S = T;
		break;
	case 2:
		S = P;
		break;
	case 3:
		S = M_T;
		break;
	}

	cout << endl<< "Enter Distation:";
	cin >> fl;
	
	k.addShip(cs, S, fl);
}

void Dialog::D_DeleteShip()
{
	if (!k.Quant())
	{
		cout << "There is no ships in the lonvoi";
		return;
	}
	
	int i = 0;
	map <string, Descriptor> ::const_iterator it1;
	map <string, Descriptor> ::const_iterator it2;
	Table T = k.getTab();
	map <string, Descriptor> Map = T.getMap();
	it1 = Map.begin();
	it2 = Map.end();
	for (; it1!= it2; ++it1)
	{
		++i;
		cout << i << ". ";
		string str=it1->first;
		
		cout << str << endl;
	}

	cout << endl << "Enter calsing:" << endl;
	string str;
	cin >> str;
	cout << endl;

	k.getTab().Delete(str);
}

void Dialog::D_Show_All()
{
	int i = 0;
	map <string, Descriptor> ::const_iterator it1;
	map <string, Descriptor> ::const_iterator it2;
	Table T = k.getTab();
	map <string, Descriptor> Map = T.getMap();
	it1 = Map.begin();
	it2 = Map.end();
	for (; it1 != it2; ++it1)
	{
		Transport *T = nullptr;
		Protection *P = nullptr;
		Military_Transport *M_T = nullptr;

		if(P = dynamic_cast<Protection*>(it1->second.getKor()))
			cout <<*P<< endl;
		if (T = dynamic_cast<Transport*>(it1->second.getKor()))
			cout << *T << endl;
		if (M_T = dynamic_cast<Military_Transport*>(it1->second.getKor()))
			cout << *M_T << endl;
	}
}

void Dialog::D_Konvoi()
{
	
	cout << "Information about konvoi:" << endl;
	cout << "Captain:" << endl;

	cout << "Name:"<<k.getAdmiral().getCaptain_Name()<<endl;

	cout << "Rang:" << k.getAdmiral().getRang() << endl;
	cout << "Experince:" << k.getAdmiral().getExp()<<endl;

	cout << "Start point:" << k.getFrom()<<endl;
	cout << "Finish point:" << k.getTo() << endl;
	cout << "Distation to finish point:" << k.getDistation() << endl;
	cout << "Quant ships point:" << k.Quant() << endl;
}

void Dialog::D_Exit()
{
	cout << "Thats all, By!" ;
	exit;
}


