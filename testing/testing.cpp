// testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "gtest\gtest.h"
#include "..\Konvoi\Konvoi.h"

Descriptor f(Ship*K)
{
	string s1 = "s1";
	Descriptor D1(K,s1,1);
	return D1;
}

TEST(Captain_test, setters)
{
	string s1 = "s1", s2 = "s2";
	Captain C;
	C.setExp(10);
	C.setCaptain_Name(s2);
	C.setRang(s1);
	EXPECT_DOUBLE_EQ(10, C.getExp());
	EXPECT_DOUBLE_EQ(1, C.getCaptain_Name() == s2);
	EXPECT_DOUBLE_EQ(1, C.getRang() == s1);
}

TEST(Captain_test, initialization)
{
	Captain C1;
	EXPECT_DOUBLE_EQ(0, C1.getExp());
	
	string s1 = "s1", s2 = "s2";
	Captain C2(s1,s2,4);
	EXPECT_DOUBLE_EQ(4, C2.getExp());
	EXPECT_DOUBLE_EQ(1, C2.getCaptain_Name()==s2);
	EXPECT_DOUBLE_EQ(1, C2.getRang()==s1);
}

TEST(Captain_test, operator_ravno)
{
	string s1 = "s1", s2 = "s2";
	Captain C1, C2(s1, s2, 4);
	C1 = C2;
	EXPECT_DOUBLE_EQ(C1.getExp(), C2.getExp());
	EXPECT_DOUBLE_EQ(1, C2.getCaptain_Name() == C1.getCaptain_Name());
	EXPECT_DOUBLE_EQ(1, C2.getRang() == C1.getRang());

	Captain C3(s2, s1, 4), C4(s1, s2, 4);
	C3= C4;
	EXPECT_DOUBLE_EQ(C3.getExp(), C4.getExp());
	EXPECT_DOUBLE_EQ(1, C3.getCaptain_Name() == C4.getCaptain_Name());
	EXPECT_DOUBLE_EQ(1, C3.getRang() == C4.getRang());
}

TEST(Transport_Test, Initialization)
{
	string s1 = "s1", s2 = "s2",s3="s3";
	Captain C1(s1, s2, 4);
	Transport T(C1,s3,1,2,3,4);
	
	EXPECT_DOUBLE_EQ(1, T.getDisplacement() == 1);
	EXPECT_DOUBLE_EQ(T.getMax_Velocity() == 2, 1);
	EXPECT_DOUBLE_EQ(T.getMax_Weight() == 4, 0);
	EXPECT_DOUBLE_EQ(T.getShip_Name() == s3,1);
	EXPECT_DOUBLE_EQ(T.getTeam()==3, 1);
	EXPECT_NO_THROW(Transport T1(C1, s3, 1, 2, 3, 4));
	EXPECT_THROW(Transport T2(C1, s3, -1, -2, -12, -99),const char*);
}

TEST(Transport_test,ravno)
{
	Transport T1;
	string s1 = "s1", s2 = "s2", s3 = "s3";
	Captain C1(s1, s2, 4);
	Transport T2(C1, s3, 1, 2, 3, 4);

	T1 = T2;
	EXPECT_DOUBLE_EQ(T1.getDisplacement()==T2.getDisplacement(),1);
	EXPECT_DOUBLE_EQ(T1.getMax_Weight() == T2.getMax_Weight(), 1);
	EXPECT_DOUBLE_EQ(T1.getShip_Name() == T2.getShip_Name(), 1);
	EXPECT_DOUBLE_EQ(T1.getTeam() == T2.getTeam(), 1);
	EXPECT_DOUBLE_EQ(T1.getWeight() == T2.getWeight(), 1);
}

TEST(Transport_test, test_CalcVel)
{
	Transport T;
	T.setVel(0);
	T.setWeight(0);
	EXPECT_DOUBLE_EQ(T.CalcVel(), 0);
}

int _tmain(int argc,_TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

