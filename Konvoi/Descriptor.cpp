#include "stdafx.h"
#include "Descriptor.h"


Descriptor::Descriptor():Kor(nullptr),Callsign(),Distation(0){}

Descriptor::Descriptor(Ship* K,  string&C, float D)
{
	Kor = K->clone();
	Callsign.assign(C);
	Distation = D;
}

Descriptor::Descriptor(const Descriptor &D)
{
	if (!D.getKor())
		Kor = nullptr;
	else
		Kor = D.getKor()->clone();
	
	Callsign.assign(D.getCallsing());
	Distation = D.getDistation();
}

Descriptor::Descriptor(const Descriptor &&D)
{
	Ship*tmp = Kor;
	Kor = D.getKor();
	setKor(tmp);

	Callsign.assign(D.getCallsing());
	Distation = D.getDistation();
}

void Descriptor::setKor(Ship *S)
{
	Kor = S->clone();

}

Descriptor & Descriptor::setCallsing( string &C)
{
	Callsign.assign(C);
	return *this;
}

Descriptor & Descriptor::setDistation(float D)
{
	Distation = D;
	return *this;
}

Ship *Descriptor::getKor() const
{
	return Kor;
}

string Descriptor::getCallsing() const
{
	return Callsign;
}

float Descriptor::getDistation() const
{
	return Distation;
}

Descriptor & Descriptor::operator=( Descriptor &D)
{
	if (!D.getKor())
		Kor = nullptr;
	else
		Kor = D.getKor()->clone();

	Callsign.assign(D.getCallsing());
	Distation = D.getDistation();
	return *this;
}

Descriptor & Descriptor::operator=(const Descriptor &&D)
{
	Ship*tmp = Kor;
	Kor = D.getKor();
	setKor(tmp);

	Callsign.assign(D.getCallsing());
	Distation = D.getDistation();
	return *this;
}

Descriptor::~Descriptor()
{
	if (Kor)
		delete Kor;
}
