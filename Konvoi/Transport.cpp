#include "stdafx.h"
#include "Transport.h"

std::ostream & Transport::print(std::ostream & c)
{
	c << "Transport name:" << getShip_Name() << std::endl << std::endl;
	c << getCaptain();
	c << "Displacement:" << getDisplacement() << std::endl;
	c << "Max Velocity:" << getMax_Velocity() << std::endl;
	c << "Team:" << getTeam() << std::endl;
	c << "Load weight:" << Load_Weight << std::endl << std::endl;
	return c;
}

Ship * Transport::clone()
{
	return new Transport(*this);
}

Transport::Transport():Load_Weight(0),Ship(){}

Transport::Transport(const Captain &C,const string&N, float D, float V, size_t T, float W)
{
	if (W > Max_Weight)
		throw "incorrect weight";
	Load_Weight=W,
	setCaptain(C);
	setTeam(T);
	setDis(D);
	setVel(V);
	setShip_Name(N);
}

Transport::Transport(Transport &Trnsprt):
	Load_Weight(Trnsprt.Load_Weight)
{
	setCaptain(Trnsprt.getCaptain());
	setTeam(Trnsprt.getTeam());
	setDis(Trnsprt.getDisplacement());
	setVel(Trnsprt.getMax_Velocity());
	setShip_Name(Trnsprt.getShip_Name());
}

Transport::~Transport(){}

float Transport::getWeight() const
{
	return Load_Weight;
}

float Transport::getMax_Weight() const
{
	return Max_Weight;
}

Transport & Transport::setWeight(float W)
{
	if (W < 0 ||W> Max_Weight)
		throw "incorrect Weight";
	Load_Weight = W;
	setVel(CalcVel());
	return *this;
}

float Transport::CalcVel()
{
	if (getMax_Velocity() == 0)
		return 0;
	float w = getMax_Velocity() - getMax_Velocity()*Load_Weight / Max_Weight;
	return w;
}

Transport & Transport::operator=(Transport &Trn)
{
	Load_Weight = Trn.Load_Weight;
	setCaptain(Trn.getCaptain());
	setDis(Trn.getDisplacement());
	setVel(Trn.getMax_Velocity());
	setShip_Name(Trn.getShip_Name());
	setTeam(Trn.getTeam());
	return *this;
}

