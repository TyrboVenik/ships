#pragma once

#include "Protection.h"
#include "Transport.h"
//#include "Weapon.h"

class Military_Transport :
	public Transport, public  Protection
{
private:
	
protected:
	std::ostream & print(std::ostream& c);
public:
	friend std::ostream & operator<<(std::ostream &os, Military_Transport&p)
	{
		return p.print(os);
	}

	Military_Transport();
	Military_Transport(Captain&, const string&, float, float, size_t, float, vector<Weapon>);

	Military_Transport(Military_Transport&);
	~Military_Transport();

	Ship *clone();

	Military_Transport &Military_Transport::operator=(Military_Transport &);
};

