#include "stdafx.h"
#include "Weapon.h"


Weapon::Weapon():
	Caliber(0),Firing_Range(0),Location(-1),Ammunition(0){}

Weapon::Weapon(size_t C, size_t F_R, int L, size_t A) : 
	Caliber(C), Firing_Range(F_R), Ammunition(A)
{
	if (Location < -1 || Location>3)
		throw "inrorrect location";
	Location=L;
}

Weapon::Weapon(const Weapon & a)
{
	Caliber = a.Caliber;
	Firing_Range = a.Firing_Range;
	Location = a.Location;
	Ammunition = a.Ammunition;
}

Weapon & Weapon::operator=(const Weapon &a)
{
	Caliber = a.Caliber;
	Firing_Range = a.Firing_Range;
	Location = a.Location;
	Ammunition = a.Ammunition;
	return *this;
}

size_t Weapon::getCaliber()const
{
	return Caliber;
}

size_t Weapon::getFiring_Range()const
{
	return Firing_Range;
}

int Weapon::getLocation()const
{
	return Location;
}

size_t Weapon::getAmmunition()const
{
	return Ammunition;
}

Weapon& Weapon::setCaliber(size_t a)
{
	if (a > MaxCaliber)
		throw "incorrect Caliber";
	Caliber = a;
	return *this;
}

Weapon&  Weapon::setFiring_Range(size_t a)
{
	if (a > MaxRange)
		throw "incorrect Range";
	Firing_Range = a;
	return *this;
}

Weapon&  Weapon::setLocation(size_t a)
{
	if (a > 4 || a<0)
		throw "incorrect Location";
	Location = a;
	return *this;
}

Weapon&  Weapon::setAmmunition(size_t a)
{
	if (a > MaxAmmunition)
		throw "incorrect Ammunition";
	Ammunition = a;
	return *this;
}

Weapon & Weapon::Shot()
{
	if (Ammunition == 0 || Location == 0)
		throw "can not shot";
	Ammunition -- ;
	return *this;
}

