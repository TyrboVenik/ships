#pragma once
#include "Captain.h"
#include <iostream>

class Ship
{
private:
	Captain Cap;
	string Ship_Name;
	float Displacement;
	float Max_Velocity;
	size_t Team;
protected:
	virtual std::ostream & print(std::ostream& c)=0;//! output function(virtual)
public:
	//! class describing ship(virtual base class)
	virtual Ship*clone()=0;//! returns a pointer to the newly created copy of the class(virtual)
	string getShip_Name()const;//! retuns name of the ship
	float getDisplacement()const;//! returns displacement of of the ship
	float getMax_Velocity()const;//! returns maximum velocity of  the ship
	size_t getTeam()const;//! returns team of the ship
	Captain& getCaptain();//! returns captain of the ship

	Ship&setCaptain(const Captain&);//! edits new captain
	Ship&setShip_Name(const string&);//! edits new ship name
	Ship&setDis(float);//! edits new Displacement of the ship
	Ship&setVel(float);//! edits new maximum velocity of the ship
	Ship&setTeam(size_t);//! edits new team of the ship

	Ship();//! default initialization constructor
	Ship(const Captain&,const string&, float, float, size_t);//! initialization constructor
	virtual~Ship();	//! destructor(virtual)
};

