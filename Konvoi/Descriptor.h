#pragma once
#include "Military_Transport.h"



class Descriptor
{
private:
	Ship* Kor;
	string Callsign;
	float Distation;
public:
	Descriptor();
	Descriptor(Ship*,  string&,float);
	Descriptor(const Descriptor&);
	Descriptor(const Descriptor&&);

	void setKor(Ship*);
	Descriptor&setCallsing(string&);
	Descriptor&setDistation(float);

	Ship*getKor()const;
	string getCallsing()const;
	float getDistation()const;

	Descriptor&operator=( Descriptor&);
	Descriptor&operator=(const Descriptor&&);
	~Descriptor();
};

