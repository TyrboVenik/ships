#include "stdafx.h"
#include "Captain.h"
#include <iostream>


std::ostream & Captain::print(std::ostream &c)
{
	c << "Captain:" << std::endl << "Name:" << getCaptain_Name() << std::endl;
	c << "Rang:" << getRang() << std::endl;
	c << "Experince:" << getExp() << std::endl<< std::endl;
	return c;
}

std::istream & Captain::input(std::istream & c)
{
	char *str;
	std::getline(c, Rang);
	std::getline(c, Captain_Name);
	c>>Experience;
	return c;
}

Captain::Captain():Rang(), Captain_Name(),Experience(0){}

Captain::Captain(const string& r,const string& n, size_t exp):
	Experience(exp)
{
	Rang.assign(r);
	Captain_Name.assign(n);
}

Captain::Captain(const Captain &C)
{
	Rang.assign(C.getRang());
	Captain_Name.assign(C.getCaptain_Name());
	Experience = C.getExp();
}

//Captain::~Captain(){}

string Captain::getRang() const
{
	return Rang;
}

string Captain::getCaptain_Name()const
{
	return Captain_Name;
}

size_t Captain::getExp()const
{
	return Experience;
}

Captain & Captain::setRang(const string str)
{
	Rang.assign(str);
	return *this;
}

Captain & Captain::setCaptain_Name(const string str)
{
	Captain_Name.append(str);
	return *this;
}

Captain & Captain::setExp(size_t exp)
{
	Experience = exp;
	return *this;
}

Captain & Captain::operator=(const Captain &C)
{
	Rang.assign(C.getRang());
	Captain_Name.assign(C.getCaptain_Name());
	Experience = C.getExp();
	return *this;
}

