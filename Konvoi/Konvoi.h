#pragma once
#include "Captain.h"
#include "Table.h"

class Konvoi
{
private:
	Table Tab;
	Captain Admiral;
	string From;
	string To;
	float Distation;
	static const int Max_Weight = 1000;
public:
	size_t Quant()const;
	
	Konvoi&addShip(string,Ship*,float);
	Konvoi&deleteShip(const string &);

	Konvoi& Upgrade(string&,size_t,string&,size_t);
	Konvoi& Load(float);//загрузить
	Konvoi& TransferWeight(const string&,const string&,float);

	Konvoi&setTab(Table&);
	Konvoi&setAdmiral(const Captain&);
	Konvoi&setFrom(const string&);
	Konvoi&setTo(const string&);
	Konvoi&setDistation(float);

	Table &getTab();
	Captain &getAdmiral();
	string getFrom()const;
	string getTo()const;
	float getDistation()const;


	Konvoi();
	Konvoi (Table&,const Captain&, string&,string&,float);
	
	Konvoi( Konvoi&);
	Konvoi&operator=(Konvoi&);

	~Konvoi();
};

/*! \mainpage My Personal Index Page
*
* \section intro_sec Introduction
*
* the program is a set of classes for working with the military convoy
*
* describes the classes to work with three types of ships:
*
* transport ship(Transport), ship escort(Protection) and military transport ship(Military_Transport)
*
*information on all ships is stored in the Table
*
*Table is stored in the main class of the Konvoi
*
*may add a new ship in the convoy
*
* \may remove the ship from the convoy
*
*\may load cargo convoy/move cargo between transport ships
*
*\may get information about each ship(Ship) convoy:
*
*\information about Captain(rank,name,experience)
*
*\ship name,displacement,maximum velocity, information about weigth or weapons(Weapon)
*
*\upgrade Transport ship to Military_Transport
*
*
*
*
* create by Vyunnikov Viktor
* MEPHI k03-121
*/