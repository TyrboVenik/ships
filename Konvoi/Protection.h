#pragma once
#include "Ship.h"
#include "Weapon.h"
//#include "my_vector.h"
#include <vector>


class Protection :
	virtual public Ship
{
private:
	vector<Weapon> Arms;
protected:
	std::ostream & print(std::ostream& c);//! output function
public:
	friend std::ostream & operator<<(std::ostream &os, Protection&p)//! output operator
	{return p.print(os);}

	Protection();//! default initialization constructor
	Protection(Captain&, const string&, float, float, size_t,const vector<Weapon>&);//! initialization constructor
	
	Protection(Protection&);//! copy constructor
	~Protection();//! destructor (virtual)

	Ship *clone();//! returns a pointer to the newly created copy of the class(virtual)

	vector<Weapon> getArms()const;//! returns ship's list of weapons(vector)

	Protection&setArms(const vector<Weapon>&);//! edits ship's list of weapons(vector)
	Protection&Load_Weapon(const Weapon&, size_t);//! establishes a new weapon on the ship
	Protection&Delete_Weapon(size_t);//! delete one wapon
	Protection&Shoot(size_t);//! shoot 

	Protection &Protection::operator=(Protection &);//! assignment operator
};

