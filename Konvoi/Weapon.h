#pragma once
class Weapon
{
private:
	static const int MaxRange = 10000;
	static const int MaxCaliber = 100;
	static const int MaxAmmunition = 50;
	size_t Caliber;
	size_t Firing_Range;
	int Location;//0-�����, 1-���, 2-����� ����, 3- ������ ����,-1-�� �����������
	size_t Ammunition;
public:
	Weapon();
	Weapon(size_t C, size_t F_R, int L, size_t A);
	Weapon(const Weapon &a);
	Weapon& operator=(const Weapon &);
	
	
	size_t getCaliber()const;
	size_t getFiring_Range()const;
	int getLocation()const;
	size_t getAmmunition()const;

	Weapon&  setCaliber(size_t);
	Weapon&  setFiring_Range(size_t);
	Weapon&  setLocation(size_t);
	Weapon&  setAmmunition(size_t);
	Weapon&  Shot();
};

