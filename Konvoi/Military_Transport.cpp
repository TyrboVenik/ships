#include "stdafx.h"
#include "Military_Transport.h"

std::ostream & Military_Transport::print(std::ostream & c)
{
	c << "Military Transport name:" << getShip_Name() << std::endl << std::endl;
	c << getCaptain();
	c << "Displacement:" << getDisplacement() << std::endl;
	c << "Max Velocity:" << getMax_Velocity() << std::endl;
	c << "Team:" << getTeam() << std::endl << std::endl;
	c << "Load weight:" << getWeight() << std::endl << std::endl;
	
	if (getArms()[0].getLocation() == 0)
	{
		c << "Stern weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[0].getCaliber() << endl;
		cout << "Fire range:" << getArms()[0].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[0].getAmmunition() << endl;
	}
	else c << "Stern weapon: not installed" << std::endl;

	if (getArms()[1].getLocation() == 1)
	{
		c << "Bow weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[1].getCaliber() << endl;
		cout << "Fire range:" << getArms()[1].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[1].getAmmunition() << endl;
	}
	else c << "Bow weapon: not installed" << std::endl;

	if (getArms()[2].getLocation() == 2)
	{
		c << "Left board weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[2].getCaliber() << endl;
		cout << "Fire range:" << getArms()[2].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[2].getAmmunition() << endl;
	}
	else c << "Left board weapon: not installed" << std::endl;

	if (getArms()[3].getLocation() == 3)
	{
		c << "Right board weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[3].getCaliber() << endl;
		cout << "Fire range:" << getArms()[3].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[3].getAmmunition() << endl;
	}
	else c << "Right board weapon: not installed" << std::endl;
	return c;
}

Military_Transport::Military_Transport() :Transport(),Protection() {}

Military_Transport::Military_Transport(Captain &C, const string&N, float D, float V, size_t T, float W,vector<Weapon> Ar) 
{
	setArms(Ar);
	setWeight(W);
	setCaptain(C);
	setTeam(T);
	setDis(D);
	setVel(V);
	setShip_Name(N);
}

Military_Transport::Military_Transport(Military_Transport &Pr) 
{
	setArms(Pr.getArms());
	setWeight(Pr.getWeight());
	setCaptain(Pr.getCaptain());
	setTeam(Pr.getTeam());
	setDis(Pr.getDisplacement());
	setVel(Pr.getMax_Velocity());
	setShip_Name(Pr.getShip_Name());
}

Military_Transport::~Military_Transport() {}

Ship * Military_Transport::clone()
{
	return new Military_Transport(*this);
}

Military_Transport & Military_Transport ::operator=( Military_Transport &Pr)
{
	setWeight(Pr.getWeight());
	setCaptain(Pr.getCaptain());
	setDis(Pr.getDisplacement());
	setVel(Pr.getMax_Velocity());
	setShip_Name(Pr.getShip_Name());
	setTeam(Pr.getTeam());
	setArms(Pr.getArms());
	return *this;
}

