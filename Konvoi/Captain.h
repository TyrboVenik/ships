#pragma once
#include<string>
#include <iostream>

using namespace std;

class Captain
{
private:
	string Rang;
	string Captain_Name;
	size_t Experience;
protected:
	std::ostream & print(std::ostream&);
	//! output function 
	//! displays full information about Captain (rank(string), name(string), experience(size_t))
	//! Takes a reference to the output stream
	//! returns a reference to the output stream
	std::istream & input(std::istream&);
	//! input function
	//! input full information about Captain (rank(string), name(string), experience(size_t))
	//! Takes a reference to the output stream
	//! returns a reference to the output stream
public:
	// class describing captain of ship or admiral of convoy
	Captain();
	//! default initialization constructor
	Captain(const string &,const string &,size_t);
	//! initialization constructor
	//! takes (captain's rank(string),name(string),experince(size_t))

	Captain(const Captain&);
	//! copy constructor

	string getRang()const;
	//! returns captain's rang(string)
	string getCaptain_Name()const;
	//! returns captain's name(string) 
	size_t getExp()const;
	//! returns experince of captain's experience(size_t)

	Captain &setRang(const string);
	//! edits captain's rang
	//! takes string
	//! returns a reference to the edited state of the class
	Captain &setCaptain_Name(const string);
	//! edits captain's name
	//! takes string
	//! returns a reference to the edited state of the class
	Captain &setExp(size_t);
	//! edits capitan's experience
	//! takes size_t
	//! returns a reference to the edited state of the class
	Captain&operator=(const Captain&);
	//! assignment operator
	//! takes Captain
	//! returns a reference to the edited state of the class
	friend std::istream & operator>>(std::istream& os, Captain& p)
	{return p.input(os);}
	//! input operator
	//! takes(a reference to the input stream(istream))
	//! returns a reference to the intput stream
	friend std::ostream & operator<<(std::ostream& os, Captain& p)
	{return p.print(os);}
	//! output operator
	//! takes(a reference to the output stream(istream))
	//! returns a reference to the output stream
};

