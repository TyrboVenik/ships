#include "stdafx.h"
#include "Protection.h"


Protection::Protection():Ship(),Arms()
{
	Weapon K;
	Arms.push_back(K);
	Arms.push_back(K);
	Arms.push_back(K);
	Arms.push_back(K);
}

Protection::Protection(Captain &C, const string&N, float D, float V, size_t T, const vector<Weapon>& Ar) :
Arms(Ar)
{
	setCaptain(C);
	setTeam(T);
	setDis(D);
	setVel(V);
	setShip_Name(N);
}

Protection::Protection(Protection &Pr):
	Arms(Pr.getArms())
{
	setCaptain(Pr.getCaptain());
	setTeam(Pr.getTeam());
	setDis(Pr.getDisplacement());
	setVel(Pr.getMax_Velocity());
	setShip_Name(Pr.getShip_Name());
}

Protection::~Protection(){}

Ship * Protection::clone()
{
	return new Protection(*this);
}

vector<Weapon> Protection::getArms()const
{
	return Arms;
}

Protection & Protection::setArms(const vector<Weapon>& W)
{
	Arms = W;
	return *this;
}

Protection & Protection::Load_Weapon(const Weapon &W, size_t i)
{
	getArms()[i] = W;
	return *this;
}

Protection & Protection::Delete_Weapon(size_t i)
{
	getArms()[i].setLocation(-1);
	return *this;
}

Protection & Protection::Shoot(size_t i)
{
	Arms[i].Shot();
	return *this;
}

std::ostream & Protection::print(std::ostream & c)
{
	c << "Protection ship name:" << getShip_Name() << std::endl << std::endl;
	c << getCaptain();
	c << "Displacement:" << getDisplacement() << std::endl;
	c << "Max Velocity:" << getMax_Velocity() << std::endl;
	c << "Team:" << getTeam() << std::endl << std::endl;
	
	if (getArms()[0].getLocation() == 0)
	{
		c << "Stern weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[0].getCaliber() << endl;
		cout << "Fire range:" << getArms()[0].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[0].getAmmunition() << endl;
	}
	else c << "Stern weapon: not installed" << std::endl;

	if (getArms()[1].getLocation() == 1)
	{
		c << "Bow weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[1].getCaliber() << endl;
		cout << "Fire range:" << getArms()[1].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[1].getAmmunition() << endl;
	}
	else c << "Bow weapon: not installed" << std::endl;

	if (getArms()[2].getLocation() == 2)
	{
		c << "Left board weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[2].getCaliber() << endl;
		cout << "Fire range:" << getArms()[2].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[2].getAmmunition() << endl;
	}
	else c << "Left board weapon: not installed" << std::endl;

	if (getArms()[3].getLocation() == 3)
	{
		c << "Right board weapon: installed" << std::endl;
		cout << "Caliber:" << getArms()[3].getCaliber() << endl;
		cout << "Fire range:" << getArms()[3].getFiring_Range() << endl;
		cout << "Fire range:" << getArms()[3].getAmmunition() << endl;
	}
	else c << "Right board weapon: not installed" << std::endl;
	
	return c;
}

Protection & Protection::operator=(Protection &Pr)
{
	setCaptain(Pr.getCaptain());
	setDis(Pr.getDisplacement());
	setVel(Pr.getMax_Velocity());
	setShip_Name(Pr.getShip_Name());
	setTeam(Pr.getTeam());
	setArms(Pr.getArms());
	return *this;
}
