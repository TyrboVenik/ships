#pragma once
#include "Ship.h"
//#include "Fun_Veloc.h"

class Transport :
	virtual public Ship
{
private:
	float Load_Weight;
	static const int Max_Weight=1000;
protected:
	virtual std::ostream & print(std::ostream& c);
public:
	friend std::ostream & operator<<(std::ostream &os, Transport&p)
	{ return p.print(os); }
	
	Ship *clone();
	Transport();
	Transport(const Captain&,const string&, float, float, size_t,float);
	Transport(Transport&);

	~Transport();

	float getWeight()const;
	float getMax_Weight()const;
	Transport& setWeight(float);
	
	virtual float CalcVel();

	Transport& operator=(Transport&);
};

