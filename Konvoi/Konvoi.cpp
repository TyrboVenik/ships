#include "stdafx.h"
#include "Konvoi.h"


Konvoi::Konvoi() 
{
}

Konvoi::Konvoi(Table &T, const Captain &C,  string &F,  string &to, float D)
{
	Tab = T;
	Admiral = C;
	From.assign(F);
	To.assign(to);
	Distation = D;
}

Konvoi::Konvoi(Konvoi &K)
{
	Tab = K.getTab();
	Admiral = K.getAdmiral();
	From.assign(K.getFrom());
	To.assign(K.getTo());
	Distation = K.getDistation();
}

Konvoi & Konvoi::operator=(Konvoi &K)
{
	Tab = K.getTab();
	Admiral = K.getAdmiral();
	From.assign(K.getFrom());
	To.assign(K.getTo());
	Distation = K.getDistation();
	return *this;
}


Konvoi::~Konvoi()
{
}

Table& Konvoi::getTab() 
{
	return Tab;
}

Captain &Konvoi::getAdmiral() 
{
	return Admiral;
}

string Konvoi::getFrom() const
{
	return From;
}

string Konvoi::getTo() const
{
	return To;
}

float Konvoi::getDistation() const
{
	return Distation;
}

size_t Konvoi::Quant() const
{
	return Tab.getNumber();
}

Konvoi & Konvoi::addShip(string C,Ship *S, float D)
{

	Tab.Add(C, S, D);
	return *this;
}

Konvoi & Konvoi::deleteShip(const string &C)
{
	Tab.Delete(C);
	return *this;
}

Konvoi & Konvoi::Upgrade( string&f,size_t pos1, string&to,size_t pos2)
{
	if (!(Tab.getMap().count(f) && Tab.getMap().count(to)))
		throw"incorrect callsing or callsings";
	
	Ship*F=Tab.getMap().find(f)->second.getKor();
	Ship*T=Tab.getMap().find(to)->second.getKor();

	Protection*From = dynamic_cast<Protection*>(F);
	Transport*To = dynamic_cast<Transport*>(T);

	if (!(From && To))
		throw "incorrect ship or ships";
	
	Weapon W1 = From->getArms()[pos1];
	vector<Weapon>Ar;
	Ar[pos1] = W1;
	Military_Transport M_T(To->getCaptain(),To->getShip_Name(),To->getDisplacement(),To->getMax_Velocity(),To->getTeam(),To->getWeight(),Ar);
	F = &M_T;
	Descriptor NM_T(F, to, Tab.getMap().find(to)->second.getDistation());
	
	delete To;

	Tab.getMap().erase(to);
	Tab.getMap().insert(pair<string, Descriptor>(to, NM_T));
	To = &M_T;
	
	From->Delete_Weapon(pos1);
	
	if (W1.getLocation() == -1)
		throw "incorrect Location or Locations";

	return *this;
}

Konvoi & Konvoi::Load(float W)
{
	int k = 0;
	Transport *T=nullptr;
	map <string, Descriptor> ::const_iterator it1;
	map <string, Descriptor> ::const_iterator it2;
	Table Ta = getTab();
	map <string, Descriptor> Map = Ta.getMap();
	it1 = Map.begin();
	it2 = Map.end();
	for (; it1 != it2; ++it1)
	{
		if (T=dynamic_cast<Transport*>(it1->second.getKor()))
		{
			++k;
			W += T->getWeight();
		}
	}
	if (k == 0 || W / k > Max_Weight)
		throw"there is not enoght space";
	W = W / k;

	it1 = Map.begin();
	it2 = Map.end();
	for (; it1 != it2; ++it1)
		if (T = dynamic_cast<Transport*>(it1->second.getKor()))
		{
			T->setWeight(W + T->getWeight());
			//Ship *Sh = T;
			//it1->second.setKor(Sh);
		}

	return *this;
}

Konvoi & Konvoi::TransferWeight(const string& F,const string& T, float W)
{
	Ship *S1 = (Tab.getDescriptor(F)).getKor();
	Ship *S2 = (Tab.getDescriptor(T)).getKor();
	
	Transport *T1 = dynamic_cast<Transport*>(S1);
	Transport *T2 = dynamic_cast<Transport*>(S2);
	
	if (!(T1&&T2))
		throw "incorrect ship or ships";

	if ((T1->getWeight() + W) > T1->getMax_Weight())
		throw "there is not enough space";
	if (T2->getWeight() < W)
		throw "there is not enough weight";

	T1->setWeight(T1->getWeight() + W);
	T2->setWeight(T2->getWeight() - W);
	return *this;
}

Konvoi & Konvoi::setTab(Table &T)
{
	Tab = T;
	return *this;
}

Konvoi & Konvoi::setAdmiral(const Captain &C)
{
	Admiral = C;
	return *this;
}

Konvoi & Konvoi::setFrom(const string &F)
{
	From.assign(F);
	return *this;
}

Konvoi & Konvoi::setTo(const string &T)
{
	To.assign(T);
	return *this;
}

Konvoi & Konvoi::setDistation(float D)
{
	Distation = D;
	return *this;
}
