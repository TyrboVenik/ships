#include "stdafx.h"
#include "Ship.h"
#include "Captain.h"


Ship::Ship():Ship_Name(),Displacement(1),Max_Velocity(0),Team(0)
{
	Captain C;
	Cap = C;
}

Ship::Ship(const Captain& C,const string& N, float D, float V, size_t T)
{	
	Ship_Name = N;

	Cap = C;

	if (!(D > 0))
		throw "incorrect Displasment";
	Displacement = D;

	if (V < 0)
		throw "incorrect Velocity";
	Max_Velocity = V;

	Team = T;
}

Ship::~Ship(){}

string Ship::getShip_Name() const
{
	return Ship_Name;
}

float Ship::getDisplacement() const
{
	return Displacement;
}

float Ship::getMax_Velocity() const
{
	return Max_Velocity;
}

size_t Ship::getTeam() const
{
	return Team;
}

Captain& Ship::getCaptain()
{
	return Cap;
}

Ship & Ship::setCaptain(const Captain &C)
{
	Cap = C;
	return *this;
}

Ship & Ship::setShip_Name(const string& str)
{
	Ship_Name = str;
	return *this;
}

Ship & Ship::setDis(float D)
{
	if (!D > 0)
		throw "incorrect Displacement";
	Displacement = D;
	return *this;
}

Ship & Ship::setVel(float V)
{
	if (V < 0)
		throw "incorrect velocity";
	Max_Velocity = V;
	return *this;
}

Ship & Ship::setTeam(size_t T)
{
	Team = T;
	return *this;
}

