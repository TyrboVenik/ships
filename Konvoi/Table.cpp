#include "stdafx.h"
#include "Table.h"


Table::Table()
{
}

Table::Table(const map<string, Descriptor>&V)
{
	for (auto v = Descr.begin(); v != V.end(); ++v)
		for (auto v1 = v; v1 != V.end(); ++v)
			if (v == v1)
				throw "incorrect Callscripts";

	auto d = Descr.begin();

	for (auto it1 = V.begin(); it1 != V.end(); ++it1,++d)
		Descr.insert(pair<string,Descriptor>(it1->first, it1->second));		
}

Table::Table(Table &T)
{
	Descr = T.getMap();
}

Table::~Table()
{
	//for (auto v = Descr.begin(); v != Descr.end(); ++v)
		//(v->second).~Descriptor();
}

map<string, Descriptor> Table::getMap() 
{
	return Descr;
}

Descriptor Table::getDescriptor(string S) const
{	
	if (!Descr.count(S))
		throw "incorrect Callsing";
	auto it = Descr.find(S);
	return it->second;
}

size_t Table::getNumber() const
{
	return Descr.size();
}

Table & Table::Add(string &C,Ship *S,float d)
{
	if (Descr.count(C) == 1)
		throw "this Call already exist";
	
	Ship* A = S->clone();
	Descriptor D(A, C, d);
	pair<string, Descriptor> p(C, D);
    Descr.insert(p);
	
	return *this;
}

Table & Table::Delete(const string &S)
{
	auto it =Descr.find(S);
	Descr.erase(it);
	return *this;
}

Table & Table::operator=(Table &T)
{
	Descr = T.getMap();
	return *this;
}



