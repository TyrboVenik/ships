#pragma once
#include "Descriptor.h"
#include<map>

class Table
{
private:
	map<string,Descriptor> Descr;
protected:
	//std::ostream & print(std::ostream& c, const Ship& A);
public:
	//friend std::ostream & operator<<(std::ostream &os, const Table &p){ return p.print(os,p); }

	Table();
	Table(const map<string, Descriptor>&);
	Table(Table&);
	~Table();
	
	map<string, Descriptor> getMap();
	Descriptor getDescriptor(string)const;
	size_t getNumber()const;

	
	
	Table& Add(string &,Ship*,float);
	Table& Delete(const string &);
	Table& operator=(Table&);
};

